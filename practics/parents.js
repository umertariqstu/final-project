var parents = /** @class */ (function () {
    function parents(a, b) {
        this.a = a;
        this.b = b;
        console.log(a, b);
    }
    Object.defineProperty(parents.prototype, "A", {
        get: function () {
            return this.a;
        },
        set: function (VAlue) {
            this.a = VAlue;
        },
        enumerable: true,
        configurable: true
    });
    return parents;
}());
